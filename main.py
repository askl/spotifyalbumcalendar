from SpotifyApiParser import SpotifyApiParser

import tkinter as tk
from tkinter.filedialog import asksaveasfilename

from ics import Calendar

root = tk.Tk()
root.withdraw()

calendar = Calendar()

apiParser = SpotifyApiParser()
anniversaries = apiParser.getAlbumAnniversaries()
calendar.events.update(anniversaries)

file_path = asksaveasfilename(title = "Select file",filetypes = [('ICS files', '*.ics'), ('All files', '*.*')], defaultextension='.ics')
with open(file_path, 'w') as file:
  file.writelines(calendar)
  file.close()