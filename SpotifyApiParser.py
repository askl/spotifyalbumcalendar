from AbstractApiParser import AbstractApiParser

from spotipy import Spotify
from spotipy.oauth2 import SpotifyOAuth

from tkinter.messagebox import showinfo, showerror
from tkinter.simpledialog import askstring

import webbrowser

class SpotifyApiParser(AbstractApiParser):
  def __init__(self):
    self.config_path = 'spotify.yml'
    self.config_props = ('client_id', 'client_secret', 'username')
    self.loadConfig()
    
    try:
      self.api = Spotify(auth_manager = SpotifyOAuth(client_id = self.config['client_id'], 
                                                     client_secret = self.config['client_secret'],
                                                     redirect_uri = 'http://localhost:8888',
                                                     scope = 'user-library-read',
                                                     username = self.config['username']))
    except Exception as e:
      showerror('Authorization failed', f'Sorry, but authorization with spotify api failed:\n{e}')

  def setUpConfig(self):
    showinfo('Setup', 'Please register your app on "https://developer.spotify.com/dashboard/"')
    webbrowser.open('https://developer.spotify.com/dashboard/', new=2)
    showinfo('Setup', 'Go to your app settings and add "http://localhost:8888" to redirect urls')
    for prop in self.config_props:
      self.config[prop] = askstring('Setup', f'Enter your spotify {prop} here:')
    self.saveConfig();

  def loadConfig(self):
    return super().loadConfig()

  def saveConfig(self):
    return super().saveConfig()

  def getAlbums(self):
    total = 1
    offset = 0
    albums = []

    while offset < total:
      response = self.api.current_user_saved_albums(limit = 50, offset = offset)
      total = response['total']
      offset += 50
      fetched_albums = [{'name' : album['album']['name'],
                        'artist' : album['album']['artists'].pop()['name'],
                        'release_date' : album['album']['release_date']}
                        for album in response['items']]
      albums.extend(fetched_albums)

    return albums

  def getAlbumAnniversary(self, album):
    return super().getAlbumAnniversary(album)
  
  def getAlbumAnniversaries(self):
    return super().getAlbumAnniversaries()