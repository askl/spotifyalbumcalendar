import yaml

from tkinter.messagebox import askyesno

from _datetime import date
from ics.event import Event
from ics.parse import ContentLine

class AbstractApiParser():
  def setUpConfig(self):
    # make sure all config properties are loaded and saved at the end
    pass

  def loadConfig(self):
    try:
      with open(self.config_path, 'r') as file:
        self.config = yaml.load(file, Loader=yaml.FullLoader) or {}
        file.close()
        if [prop for prop in self.config_props if prop not in self.config]:
          raise FileNotFoundError
        if askyesno('Setup', 'Do you want to repeat setup?'):
          self.setUpConfig()
    except:
      self.setUpConfig()

  def saveConfig(self):
    with open(self.config_path, 'w') as file:
      file.write(yaml.dump(self.config))
      file.close()
  
  def getAlbums(self):
    # return a list of albums as dictionaries with fields: 'name', 'artist' and 'release_date'
    pass

  def getAlbumAnniversaries(self):
    return [self.getAlbumAnniversary(album) for album in self.getAlbums()]

  def getAlbumAnniversary(self, album):
    anniversary_date = None
    try:
      anniversary_date = date.fromisoformat(album['release_date'])
    except Exception as e:
      print(album)
      print(e)
      return

    anniversary = Event()
    anniversary.name = f"{album['name']} by {album['artist']} in {album['release_date']}"
    anniversary.begin = anniversary_date
    anniversary.make_all_day()
    anniversary.extra.append(ContentLine(name = 'RRULE', value = 'FREQ=YEARLY'))

    return anniversary